import avs.AerialVehicleDetails;
import avs.FlightStatus;
import avs.jets.Jet;
import avs.jets.models.F15;
import avs.operation.units.AttackUnit;
import avs.operation.units.BDAUnit;
import avs.operation.units.IntelUnit;
import avs.operation.units.types.CameraType;
import avs.operation.units.types.RocketType;
import avs.operation.units.types.SensorType;
import avs.uav.Hermes;
import avs.uav.UAV;
import avs.uav.models.Kochav;
import avs.uav.models.Zik;
import entities.Coordinates;
import missions.AttackMission;
import missions.BDAMission;
import missions.IntelligenceMission;
import missions.Mission;

public class Main {

    public static void main(String[] args) {

        firstFlow();
        secondFlow();
        thirdFlow();

    }

    private static void firstFlow() {

        Hermes zik = new Zik(
                instantiateDetails(),
                new IntelUnit(SensorType.INFRA_RED),
                new BDAUnit(CameraType.NIGHT_VISION)
        );

        String pilot = "Emil";
        String region = "Tel-Aviv";

        Mission mission = new IntelligenceMission(
                new Coordinates(32.115364882650155, 34.82001107029173),
                pilot,
                zik,
                region
        );

        mission.begin();
        mission.cancel();
    }

    private static void secondFlow() {

        Jet f15 = new F15(
                instantiateDetails(),
                new AttackUnit(100, RocketType.PYTHON),
                new IntelUnit(SensorType.EL_INT)
        );

        String pilot = "Emil";
        String target = "Afeka";

        Mission mission = new AttackMission(
                new Coordinates(32.115364882650155, 34.82001107029173),
                pilot, f15, target);


        mission.begin();
        mission.finish();
    }

    private static void thirdFlow() {

        UAV star = new Kochav(
                instantiateDetails(),
                new IntelUnit(SensorType.EL_INT),
                new BDAUnit(CameraType.REGULAR),
                new AttackUnit(1, RocketType.AMRAM)
        );

        String pilot = "Emil";
        String objective = "Afeka";

        Mission mission = new BDAMission(
                new Coordinates(32.115364882650155, 34.82001107029173),
                pilot,
                star,
                objective
        );

        mission.begin();
        mission.finish();
    }

    private static AerialVehicleDetails instantiateDetails() {
        return new AerialVehicleDetails(
                0,
                FlightStatus.READY,
                new Coordinates(30.632350627916306, 34.789478562842525));
    }

}
