package avs;


import avs.operation.units.AttackUnit;
import avs.operation.units.BDAUnit;
import avs.operation.units.IntelUnit;
import entities.Coordinates;

import java.util.Optional;

public abstract class AerialVehicle {

    protected AerialVehicleDetails details;
    protected int airborneDurationLimit;
    protected AttackUnit attackUnit;
    protected BDAUnit bdaUnit;
    protected IntelUnit intelUnit;

    public AerialVehicle(AerialVehicleDetails details) {
        this.details = details;
    }

    public void flyTo(Coordinates destination) {
        if(isReady()) {
            System.out.println("Flying to: " + destination);
            details.setFlightStatus(FlightStatus.AIRBORNE);
        }
        else
            System.out.println("Aerial Vehicle isn't ready to fly");
    }

    private boolean isReady() {
        return details.getFlightStatus() == FlightStatus.READY;
    }

    public void land(Coordinates destination) {
        performLanding(destination);
    }

    private void performLanding(Coordinates destination) {
        System.out.println("Landing on: " + destination);
        check();
    }

    private void check() {
        int timeSinceLastFix = details.getTimeSinceLastFixInHours();
        if(timeSinceLastFix >= airborneDurationLimit) {
            details.setFlightStatus(FlightStatus.NOT_READY);
            repair();
        } else {
            details.setFlightStatus(FlightStatus.READY);
        }
    }

    private void repair() {
        details.setTimeSinceLastFixInHours(0);
        details.setFlightStatus(FlightStatus.READY);
    }

    public void setAirborneDurationLimit(int airborneDurationLimit) {
        this.airborneDurationLimit = airborneDurationLimit;
    }

    public void returnToBase() {
        performLanding(details.getBaseLocationCoordinates());
    }

    public AerialVehicleDetails getDetails() {
        return details;
    }

    public AttackUnit getAttackUnit() {
        return attackUnit;
    }

    public BDAUnit getBdaUnit() {
        return bdaUnit;
    }

    public IntelUnit getIntelUnit() {
        return intelUnit;
    }
}
