package avs.uav.models;

import avs.AerialVehicleDetails;
import avs.operation.units.AttackUnit;
import avs.operation.units.BDAUnit;
import avs.operation.units.IntelUnit;
import avs.uav.Haron;

import java.util.Optional;

public class Eitan extends Haron {

    public Eitan(AerialVehicleDetails details, IntelUnit intelUnit, AttackUnit attackUnit) {
        super(details);
        super.intelUnit = intelUnit;
        super.attackUnit = attackUnit;
    }
}
