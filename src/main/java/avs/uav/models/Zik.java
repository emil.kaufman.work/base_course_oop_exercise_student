package avs.uav.models;


import avs.AerialVehicleDetails;
import avs.operation.units.AttackUnit;
import avs.operation.units.BDAUnit;
import avs.operation.units.IntelUnit;
import avs.uav.Hermes;

import java.util.Optional;

public class Zik extends Hermes {
    public Zik(AerialVehicleDetails details, IntelUnit intelUnit, BDAUnit bdaUnit) {
        super(details);
        super.intelUnit = intelUnit;
        super.bdaUnit = bdaUnit;
    }
}