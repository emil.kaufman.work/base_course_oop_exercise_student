package avs.uav.models;


import avs.AerialVehicleDetails;
import avs.operation.units.AttackUnit;
import avs.operation.units.BDAUnit;
import avs.operation.units.IntelUnit;
import avs.uav.Haron;

import java.util.Optional;

public class Shoval extends Haron {
    public Shoval(AerialVehicleDetails details, IntelUnit intelUnit, BDAUnit bdaUnit, AttackUnit attackUnit) {
        super(details);
        super.intelUnit = intelUnit;
        super.bdaUnit = bdaUnit;
        super.attackUnit = attackUnit;
    }
}

