package avs.uav;

import avs.AerialVehicle;
import avs.AerialVehicleDetails;
import avs.FlightStatus;
import entities.Coordinates;

public abstract class UAV extends AerialVehicle {

    public UAV(AerialVehicleDetails details) {
        super(details);
    }

    public String hoverOverLocation(Coordinates destination) {
        details.setFlightStatus(FlightStatus.AIRBORNE);
        String report = "Hovering over: " + destination;
        System.out.println(report);
        return report;
    }

}
