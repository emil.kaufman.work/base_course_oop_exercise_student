package avs.uav;

import avs.AerialVehicleDetails;

public abstract class Haron extends UAV{

    private final int AIRBORNE_DURATION_LIMIT = 150;

    public Haron(AerialVehicleDetails details) {
        super(details);
        setAirborneDurationLimit(AIRBORNE_DURATION_LIMIT);
    }
}
