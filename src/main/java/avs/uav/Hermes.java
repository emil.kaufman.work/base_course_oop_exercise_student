package avs.uav;

import avs.AerialVehicleDetails;

public abstract class Hermes extends UAV{

    private final int AIRBORNE_DURATION_LIMIT = 100;

    public Hermes(AerialVehicleDetails details) {
        super(details);
        setAirborneDurationLimit(AIRBORNE_DURATION_LIMIT);
    }
}
