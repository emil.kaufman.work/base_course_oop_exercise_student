package avs;

import entities.Coordinates;

public class AerialVehicleDetails {

    private int timeSinceLastFixInHours;
    private FlightStatus flightStatus;
    private Coordinates baseLocationCoordinates;

    public AerialVehicleDetails(int timeSinceLastFixInHours, FlightStatus flightStatus, Coordinates baseLocationCoordinates) {
        this.timeSinceLastFixInHours = timeSinceLastFixInHours;
        this.flightStatus = flightStatus;
        this.baseLocationCoordinates = baseLocationCoordinates;
    }

    public int getTimeSinceLastFixInHours() {
        return timeSinceLastFixInHours;
    }

    public void setTimeSinceLastFixInHours(int timeSinceLastFixInHours) {
        this.timeSinceLastFixInHours = timeSinceLastFixInHours;
    }

    public FlightStatus getFlightStatus() {
        return flightStatus;
    }

    public void setFlightStatus(FlightStatus flightStatus) {
        this.flightStatus = flightStatus;
    }

    public Coordinates getBaseLocationCoordinates() {
        return baseLocationCoordinates;
    }

    public void setBaseLocationCoordinates(Coordinates baseLocationCoordinates) {
        this.baseLocationCoordinates = baseLocationCoordinates;
    }
}
