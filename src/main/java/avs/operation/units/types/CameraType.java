package avs.operation.units.types;

public enum CameraType {

    REGULAR, THERMAL, NIGHT_VISION

}
