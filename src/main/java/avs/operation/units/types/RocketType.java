package avs.operation.units.types;

public enum RocketType {
    PYTHON, AMRAM, SPICE250
}
