package avs.operation.units;

import avs.operation.units.types.RocketType;

public class AttackUnit extends MissionUnit{

    private int rocketsAmount;
    private RocketType rocketType;

    public AttackUnit(int rocketsAmount, RocketType rocketType) {
        this.rocketsAmount = rocketsAmount;
        this.rocketType = rocketType;
    }

    public int getRocketsAmount() {
        return rocketsAmount;
    }

    public void setRocketsAmount(int rocketsAmount) {
        this.rocketsAmount = rocketsAmount;
    }

    public RocketType getRocketType() {
        return rocketType;
    }

    public void setRocketType(RocketType rocketType) {
        this.rocketType = rocketType;
    }
}
