package avs.operation.units;

import avs.operation.units.types.SensorType;

public class IntelUnit extends MissionUnit{

    SensorType sensorType;

    public IntelUnit(SensorType sensorType) {
        this.sensorType = sensorType;
    }

    public SensorType getSensorType() {
        return sensorType;
    }

    public void setSensorType(SensorType sensorType) {
        this.sensorType = sensorType;
    }
}
