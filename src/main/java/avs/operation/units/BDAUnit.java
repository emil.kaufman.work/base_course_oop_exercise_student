package avs.operation.units;

import avs.operation.units.types.CameraType;

public class BDAUnit extends MissionUnit{

    CameraType cameraType;

    public BDAUnit(CameraType cameraType) {
        this.cameraType = cameraType;
    }

    public CameraType getCameraType() {
        return cameraType;
    }

    public void setCameraType(CameraType cameraType) {
        this.cameraType = cameraType;
    }
}
