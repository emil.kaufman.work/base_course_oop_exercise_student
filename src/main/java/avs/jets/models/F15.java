package avs.jets.models;


import avs.AerialVehicleDetails;
import avs.jets.Jet;
import avs.operation.units.AttackUnit;
import avs.operation.units.IntelUnit;

import java.util.Optional;

public class F15 extends Jet {

    public F15(AerialVehicleDetails details, AttackUnit attackUnit, IntelUnit intelUnit) {
        super(details);
        super.attackUnit = attackUnit;
        super.intelUnit =  intelUnit;
    }
}
