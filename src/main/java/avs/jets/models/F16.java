package avs.jets.models;


import avs.AerialVehicleDetails;
import avs.jets.Jet;
import avs.operation.units.AttackUnit;
import avs.operation.units.BDAUnit;
import avs.operation.units.IntelUnit;

import java.util.Optional;

public class F16 extends Jet {

    public F16(AerialVehicleDetails details, AttackUnit attackUnit, BDAUnit bdaUnit) {
        super(details);
        super.attackUnit = attackUnit;
        super.bdaUnit =  bdaUnit;
    }
}
