package avs.jets;

import avs.AerialVehicle;
import avs.AerialVehicleDetails;
import avs.operation.units.AttackUnit;
import entities.Coordinates;

public abstract class Jet extends AerialVehicle {

    protected final int AIRBORNE_DURATION_LIMIT = 250;

    public Jet(AerialVehicleDetails details) {
        super(details);
        setAirborneDurationLimit(AIRBORNE_DURATION_LIMIT);
    }

}
