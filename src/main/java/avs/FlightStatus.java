package avs;

public enum FlightStatus {
    READY, NOT_READY, AIRBORNE
}
