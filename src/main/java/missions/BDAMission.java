package missions;

import avs.AerialVehicle;
import entities.Coordinates;

public class BDAMission extends Mission{

    private String objective;

    public BDAMission(Coordinates destination, String pilotName, AerialVehicle aerialVehicle, String objective) {
        super(destination, pilotName, aerialVehicle);
        this.objective = objective;
    }

    @Override
    protected String executeMission() {
        return
                pilotName + ": " +
                        aerialVehicle.getClass().getSimpleName()
                        + " taking pictures of " + objective
                        + " with: " + aerialVehicle.getBdaUnit().getCameraType();
    }
}
