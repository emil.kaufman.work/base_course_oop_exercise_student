package missions;

import avs.AerialVehicle;
import entities.Coordinates;

public abstract class Mission{

    protected Coordinates destination;
    protected String pilotName;
    protected AerialVehicle aerialVehicle;

    public Mission(Coordinates destination, String pilotName, AerialVehicle aerialVehicle) {
        this.destination = destination;
        this.pilotName = pilotName;
        this.aerialVehicle = aerialVehicle;
    }

    public void begin() {
        System.out.println("Beginning Mission!");
        aerialVehicle.flyTo(destination);
    }

    public void cancel(){
        System.out.println("Abort Mission!");
        aerialVehicle.returnToBase();
    }

    public void finish() {
        System.out.println(executeMission());
        aerialVehicle.returnToBase();
        System.out.println("Finish Mission!");
    }

    protected abstract String executeMission();

}
