package missions;

import avs.AerialVehicle;
import entities.Coordinates;

public class AttackMission extends Mission{

    private String target;

    public AttackMission(Coordinates destination, String pilotName, AerialVehicle aerialVehicle, String target) {
        super(destination, pilotName, aerialVehicle);
        this.target = target;
    }

    @Override
    protected String executeMission() {
        return
                pilotName + ": "
                + aerialVehicle.getClass().getSimpleName()
                + " attacking " + target
                + " with: " + aerialVehicle.getAttackUnit().getRocketType()
                + "x" + aerialVehicle.getAttackUnit().getRocketsAmount();
    }
}
