package missions;

import avs.AerialVehicle;
import entities.Coordinates;

public class IntelligenceMission extends Mission{

    private String region;

    public IntelligenceMission(Coordinates destination, String pilotName, AerialVehicle aerialVehicle, String region) {
        super(destination, pilotName, aerialVehicle);
        this.region = region;
    }

    @Override
    protected String executeMission() {
        return
                pilotName + ": "
                        + aerialVehicle.getClass().getSimpleName()
                        + " collecting Data in  " + region
                        + " with: " + aerialVehicle.getIntelUnit().getSensorType();
    }
}
